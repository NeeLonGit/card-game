Feature: Navigate the lobby

Scenario: Leave the lobby
  Given I am at the lobby page
  When I leave the lobby
  Then I am not in the lobby

Scenario: Join a new game
  Given I am at the lobby page
  When I join a new game
  Then I am not in the lobby
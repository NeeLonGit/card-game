Feature: Select a name

Scenario: select a non-empty name
Given I am at the name selector page
And I fill in a name
When I submit the name
Then I am not in the name selector

Scenario: select an empty name
Given I am at the name selector page
But I do not fill in a name
When I submit the name
Then I am in the name selector
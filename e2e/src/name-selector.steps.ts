import { AppPo } from "../../src/app/app.po";
import { $, browser } from 'protractor';
import { Given, When, Then } from 'cucumber';
import { expect } from 'chai';

const appPo: AppPo = new AppPo($('app-root'));

Given(/^I am at the name selector page$/, async () => {
    await appPo.happyFlowToNameSelector();
});

When(/^I fill in a name$/, async () => {
    await appPo.getNameSelectorPo().setName('name');
});

When(/^I do not fill in a name$/, async () => {
    await appPo.getNameSelectorPo().setName('');
});

When(/^I submit the name$/, async () => {
    await appPo.getNameSelectorPo().submitName();
});

Then(/^I am in the name selector$/, async () => {
    expect(await appPo.getNameSelectorPo().isDisplayed()).to.equal(true);
})

Then(/^I am not in the name selector$/, async () => {
    await browser.waitForAngularEnabled(false);
    expect(await appPo.getNameSelectorPo().isNotPresent()).to.equal(true);
    await browser.waitForAngularEnabled(true);
})

import { LobbyPo } from "../../src/app/lobby/lobby.po";
import { AppPo } from "../../src/app/app.po";
import { $, browser } from 'protractor';
import { Given, When, Then } from 'cucumber';
import { expect } from 'chai';

const appPo: AppPo = new AppPo($('app-root'));

Given(/^I am at the lobby page$/, async () => {
    await appPo.happyFlowToLobby();
    await browser.waitForAngularEnabled(false);
});

When(/^I leave the lobby$/, async () => {
    await appPo.getLobbyPo().closeLobby();
});

When(/^I join a new game$/, async () => {
    const numberOfGames: number = await appPo.getLobbyPo().countJoinableTables();
    await appPo.getLobbyPo().addNewTable();
    await browser.wait(async () => numberOfGames+1 === await appPo.getLobbyPo().countJoinableTables());
    await appPo.getLobbyPo().joinTable((await appPo.getLobbyPo().countJoinableTables())-1);
});

Then(/^I am not in the lobby$/, async () => {
    await browser.waitForAngular();
    expect(await appPo.getLobbyPo().isNotPresent()).to.equal(true);
    await browser.waitForAngularEnabled(true);
})
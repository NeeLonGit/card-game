import { ElementFinder } from 'protractor';

export class NameSelectorPo {
     private nameInput: ElementFinder;
     private acceptButton: ElementFinder;

    constructor(private container: ElementFinder) {
        this.nameInput = this.container.$('input');
        this.acceptButton = this.container.$('button');
    }

    public async happyFlow(): Promise<void> {
        await this.setName('name');
        await this.submitName();
    }

    public async setName(name: string): Promise<void> {
        await this.nameInput.clear();
        await this.nameInput.sendKeys(name);
    }

    public async submitName(): Promise<void> {
        await this.acceptButton.click();
    }

    public async isDisplayed(): Promise<boolean> {
        return await this.container.isDisplayed();
    }

    public async isNotPresent(): Promise<boolean> {
        return !await this.container.isPresent();
    }
}
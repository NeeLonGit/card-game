import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NameSelectorComponent } from './name-selector.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [NameSelectorComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [NameSelectorComponent]
})
export class NameSelectorModule { }

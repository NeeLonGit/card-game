import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-name-selector',
  templateUrl: './name-selector.component.html',
  styleUrls: ['./name-selector.component.scss']
})
export class NameSelectorComponent implements OnInit {

  @Input()
  public name: string = '';
  @Output()
  public nameSelected: EventEmitter<string> = new EventEmitter<string>();
  public formGroup: FormGroup;

  constructor() { }

  ngOnInit(): void {
    this.formGroup = new FormGroup({
      name: new FormControl(this.name, [Validators.required])
    });
  }

  setName(): void {
    if(this.formGroup.valid) {
      this.nameSelected.emit(this.formGroup.controls.name.value);
    }
  }
}

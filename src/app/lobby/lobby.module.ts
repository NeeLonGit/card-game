import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LobbyComponent } from './lobby.component';
import { TableModule } from '../table/table.module';

@NgModule({
  declarations: [LobbyComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule
  ],
  exports: [LobbyComponent]
})
export class LobbyModule { }

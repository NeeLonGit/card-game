import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TableResource } from '../table/table.resource';
import { Table } from '../table/table.model';
import { timer, Observable, Subscription } from 'rxjs';
import { switchMap, tap, map } from 'rxjs/operators';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss']
})
export class LobbyComponent implements OnInit, OnDestroy {
  
  @Output()
  public choseTable: EventEmitter<string> = new EventEmitter<string>();
  @Output()
  public leftLobby: EventEmitter<void> = new EventEmitter<void>();

  public tables: Table[] = [];

  private subscriptions: Subscription = new Subscription();

  constructor(private tableResource: TableResource) { }

  public ngOnInit(): void {
    this.subscriptions.add(timer(0, 1000).pipe(
      switchMap(() => this.refreshAvailableTables())
    ).subscribe());
  }

  public ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  public createTable(): void {
    this.tableResource.createTable().subscribe();
  }

  public refreshAvailableTables(): Observable<void> {
    return this.tableResource.getTables().pipe(
      map(
      (tables: Table[]) => {
        this.tables = tables;
      }));
  } 

  public chooseTable(tableId: string): void {
    this.choseTable.emit(tableId);
  }

  public leaveLobby(): void {
    this.leftLobby.emit();
  }
}

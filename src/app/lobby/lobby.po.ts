import { ElementFinder, ElementArrayFinder, browser } from 'protractor';

export class LobbyPo {

    private closeButton: ElementFinder;
    private joinableTables: ElementArrayFinder;
    private createTableButton: ElementFinder;

    constructor(private container: ElementFinder) {
        this.closeButton = this.container.$$('button').get(0);
        this.joinableTables = this.container.$$('div');
        this.createTableButton = this.container.$$('button').get(1);
    }

    public async happyFlow(): Promise<void> {
        const numberOfGames: number = await this.countJoinableTables();
        await this.addNewTable();
        await browser.wait(async () => numberOfGames+1 === await this.countJoinableTables());
        await this.joinTable((await this.countJoinableTables())-1);
    }

    public async closeLobby(): Promise<void> {
        return await this.closeButton.click();
    }

    public async addNewTable(): Promise<void> {
        return await this.createTableButton.click();
    }

    public async joinTable(index: number ): Promise<void> {
        return await this.joinableTables.get(index).click();
    }

    public async countJoinableTables(): Promise<number> {
        return (await this.joinableTables).length;
    }

    public async isDisplayed(): Promise<boolean> {
        return await this.container.isDisplayed();
    }

    public async isNotPresent(): Promise<boolean> {
        return !await this.container.isPresent();
    }
}
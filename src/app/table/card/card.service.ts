import { Injectable } from '@angular/core';
import { Card } from './card.model';

@Injectable({
  providedIn: 'root'
})
export class CardService {

  constructor() {
  }

  public coupeCards(cards: Card[]): Card[] {
    const splitIndex: number = Math.floor(cards.length* (Math.random() + Math.random() + Math.random() + Math.random()) / 4);
    return cards.slice(splitIndex).concat(cards.slice(0, splitIndex));
  }

  public shuffleCards(cards: Card[]): Card[] {
    for (let i = 0; i < 1000; i++) {
      var location1 = Math.floor((Math.random() * cards.length));
      var location2 = Math.floor((Math.random() * cards.length));
      var tmp = cards[location1];

      cards[location1] = cards[location2];
      cards[location2] = tmp;
    }
    return cards;
  }

  public sortCards(cards: Card[]): Card[] {
    return cards.sort((card1: Card, card2: Card) => card1.suit - card2.suit || card1.name - card2.name);
  }
}

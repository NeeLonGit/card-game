export interface Card {
    suit: Suit;
    name: CardName;
}

export enum Suit {
    SPADES,
    HEARTS,
    CLUBS,
    DIAMONDS
}

export enum CardName {
    TWO,
    THREE,
    FOUR,
    FIVE,
    SIX,
    SEVEN,
    EIGHT,
    NINE,
    TEN,
    JACK,
    QUEEN,
    KING,
    ACE
}
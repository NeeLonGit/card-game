import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Card, Suit, CardName } from './card.model';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnChanges {

  @Input()
  card: Card;
  @Input()
  isOpen: boolean = false;
  @Input()
  orientation: 'horizontal' | 'vertical' = 'vertical';
  @Output()
  clicked: EventEmitter<void> = new EventEmitter<void>();

  public suitSymbol: '♠' | '♥' | '♦' | '♣';
  public valueSymbol: '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | '10' | 'J' | 'Q' | 'K' | 'A';

  constructor() { }

  ngOnChanges(): void {
    this.suitSymbol = this.getSuitSymbol(this.card);
    this.valueSymbol = this.getValueSymbol(this.card);
  }

  emitClicked(): void {
    this.clicked.emit();
  }

  private getSuitSymbol(card: Card): '♠' | '♥' | '♦' | '♣' {
    switch(card.suit) {
      case Suit.SPADES: return '♠';
      case Suit.HEARTS: return '♥';
      case Suit.DIAMONDS: return '♦';
      case Suit.CLUBS: return '♣';
      default: return null;
    }
  }

  private getValueSymbol(card: Card): '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | '10' | 'J' | 'Q' | 'K' | 'A' {
    switch(card.name) {
      case CardName.TWO: return '2';
      case CardName.THREE: return '3';
      case CardName.FOUR: return '4';
      case CardName.FIVE: return '5';
      case CardName.SIX: return '6';
      case CardName.SEVEN: return '7';
      case CardName.EIGHT: return '8';
      case CardName.NINE: return '9';
      case CardName.TEN: return '10';
      case CardName.JACK: return 'J';
      case CardName.QUEEN: return 'Q';
      case CardName.KING: return 'K';
      case CardName.ACE: return 'A';
      default: return null;
    }
  }
}

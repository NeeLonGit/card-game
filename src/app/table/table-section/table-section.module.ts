import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardModule } from '../card/card.module';
import { TableSectionComponent } from './table-section.component';



@NgModule({
  declarations: [TableSectionComponent],
  imports: [
    CardModule,
    CommonModule
  ],
  exports: [TableSectionComponent]
})
export class TableSectionModule { }

import { Card } from '../card/card.model';

export interface TableSection {
    openCards: Card[];
    closedCards: Card[];
}
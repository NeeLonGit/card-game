import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TableSection } from './table-section.model';
import { CardService } from '../card/card.service';
import { Card } from '../card/card.model';

@Component({
  selector: 'app-table-section',
  templateUrl: './table-section.component.html',
  styleUrls: ['./table-section.component.scss']
})
export class TableSectionComponent implements OnInit {

  @Input()
  name: string;
  @Input()
  tableSection: TableSection;

  @Output()
  clickedOpenCard: EventEmitter<Card> = new EventEmitter<Card>();
  @Output()
  clickedClosedCards: EventEmitter<void> = new EventEmitter<void>();

  constructor(private cardService: CardService) { }

  ngOnInit(): void {
  }

  openCardClicked(card: Card): void {
    this.clickedOpenCard.emit(card);
  }
  closedCardsClicked(): void {
    this.clickedClosedCards.emit();
  }
}

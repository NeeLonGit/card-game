import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Suit, CardName, Card } from './card/card.model';
import { Table } from './table.model';
import { TableResource } from './table.resource';
import { Observable, timer, Subscription } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { CardService } from './card/card.service';
import { TableSection } from './table-section/table-section.model';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, OnDestroy {

  @Input()
  public tableId: string;
  @Input()
  public playerId: string;
  @Input()
  public playerName: string;
  public table: Table;
  public playerIds: string[];
  @Output()
  public leftTable: EventEmitter<void> = new EventEmitter<void>();

  private subscriptions: Subscription = new Subscription();

  constructor(private tableResource: TableResource,
              private cardService: CardService) { }

  ngOnInit(): void {
    this.subscriptions.add(timer(0, 1000).pipe(
      switchMap(() => this.updateTable())
    ).subscribe(
      (table: Table) => {
        if (table.players[this.playerId] && table.players[this.playerId].name !== this.playerName){
          table.players[this.playerId].name = this.playerName;
          this.sendTable();
        }
      }
    ));
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  public mainSectionHasCards(): boolean {
    return this.table.mainSection.closedCards.length !== 0 || this.table.mainSection.openCards.length !== 0;
  }

  public sideSectionHasCards(): boolean {
    return this.table.sideSection.closedCards.length !== 0 || this.table.sideSection.openCards.length !== 0;
  }

  public updateTable(): Observable<Table> {
    return this.tableResource.getTable(this.tableId).pipe(
      tap(
      (table: Table) => {
        this.table = table;
        this.playerIds = Object.keys(table.players);
      })
    );
  }

  public leaveTable(): void {
    this.leftTable.emit();
  }

  public hasTable(): boolean {
    return !!this.table;
  }

  public coupCards(): void {
    this.table.mainSection.closedCards = this.cardService.coupeCards(this.table.mainSection.closedCards);
    this.sendTable();
  }

  public shuffleCards(): void {
    this.table.mainSection.closedCards = this.cardService.shuffleCards(this.table.mainSection.closedCards);
    this.sendTable();
  }

  public playCard(card: Card): void {
    const index = this.table.players[this.playerId].cards.indexOf(card);
    this.table.players[this.playerId].cards.splice(index, 1);
    this.table.playerSections[this.playerId].openCards.push(card);
    this.sendTable();
  }

  public takeCardFromPlayerSection(card: Card, playerId: string) {
    const index: number = this.table.playerSections[playerId].openCards.indexOf(card);
    this.table.playerSections[playerId].openCards.splice(index, 1);
    this.table.players[this.playerId].cards.push(card);
    this.sendTable();
  }

  public takeCardFromMainSection(card: Card) {
    const index: number = this.table.mainSection.openCards.indexOf(card);
    this.table.mainSection.openCards.splice(index, 1);
    this.table.players[this.playerId].cards.push(card);
    this.sendTable();
  }

  public takeCardFromSideSection(card: Card) {
    const index: number = this.table.sideSection.openCards.indexOf(card);
    this.table.sideSection.openCards.splice(index, 1);
    this.table.players[this.playerId].cards.push(card);
    this.sendTable();
  }

  public sortHand(playerId: string): void {
    this.table.players[playerId].cards = this.cardService.sortCards(this.table.players[playerId].cards);
    this.sendTable();
  }

  private sendTable(): void {
    this.tableResource.setTable(this.table, this.playerId).subscribe();
  }

  public collectPlayerClosedCards(playerId: string): void {
    this.table.players[this.playerId].cards = this.table.players[this.playerId].cards.concat(this.table.playerSections[playerId].closedCards);
    this.table.playerSections[playerId].closedCards = [];
    this.sendTable();
  }

  public collectMainClosedCards(): void {
    this.table.players[this.playerId].cards = this.table.players[this.playerId].cards.concat(this.table.mainSection.closedCards);
    this.table.mainSection.closedCards = [];
    this.sendTable();
  }

  public collectSideClosedCards(): void {
    this.table.players[this.playerId].cards = this.table.players[this.playerId].cards.concat(this.table.sideSection.closedCards);
    this.table.sideSection.closedCards = [];
    this.sendTable();
  }

  public collectOpenCards(): void {
    const cards: Card[] = this.playerIds
      .map(playerId => {
        const playerCards = this.table.playerSections[playerId].openCards.splice(0, this.table.playerSections[playerId].openCards.length);
        return playerCards;
      }).
      reduce((p, c) => p.concat(c), []);
    this.table.playerSections[this.playerId].closedCards = this.table.playerSections[this.playerId].closedCards.concat(cards);
    this.sendTable();
  }

  public returnAllCards(): void {
    let cards: Card[] = this.playerIds
      .map(playerId => this.emptySection(this.table.playerSections[playerId]))
      .reduce((p, c) => p.concat(c), []);
    cards = cards.concat(this.emptySection(this.table.sideSection));
    this.table.mainSection.closedCards = this.table.mainSection.closedCards.concat(cards);
    this.sendTable();
  }

  public dealCards(): void {
    let cards: Card[] = this.table.mainSection.closedCards;
    const playerIndex = this.playerIds.indexOf(this.playerId);
    const playerAmount = this.playerIds.length;
      for(let index = playerIndex+1; index < playerIndex+playerAmount+1; index++) {
        this.table.playerSections[this.playerIds[index%playerAmount]].closedCards = this.table.playerSections[this.playerIds[index%playerAmount]].closedCards.concat(cards.splice(0, 7));
      }
      for(let index = playerIndex+1; index < playerIndex+playerAmount+1; index++) {
        this.table.playerSections[this.playerIds[index%playerAmount]].closedCards = this.table.playerSections[this.playerIds[index%playerAmount]].closedCards.concat(cards.splice(0, 6));
      }
      this.sendTable();
  }

  private emptySection(tableSection: TableSection): Card[] {
    let cards: Card[] = [];
    const openLength = tableSection.openCards.length;
    if(openLength > 0) {
      cards = cards.concat(tableSection.openCards.splice(0, openLength));
    }
    const closedLength = tableSection.closedCards.length;
    if(closedLength > 0) {
      cards = cards.concat(tableSection.closedCards.splice(0, closedLength));
    }
    return cards;
  }
}
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators'
import { Table } from './table.model';

@Injectable({
    providedIn: 'root'
  })
export class TableResource {

    private sequence: number;

    constructor(private httpClient: HttpClient) {}

    public createTable(): Observable<string> {
        return this.httpClient.post<{[id: string]: string}>('table', null).pipe(
            map((obj: {[id: string]: string}) => obj.id)
        );
    }

    public getTables(): Observable<Table[]> {
        return this.httpClient.get<Table[]>('table');
    }

    public getTable(tableId: string): Observable<Table> {
        return this.httpClient.get<{table: Table, sequence: number}>(`table/${tableId}`).pipe(
            map(obj => {
                this.sequence = obj.sequence;
                return obj.table;
            })
        );
    }

    public setTable(table: Table, playerId): Observable<void> {
        const obj = {
            player: table.players[playerId],
            mainSection: table.mainSection,
            sideSection: table.sideSection,
            playerSections: table.playerSections
        };
        return this.httpClient.put<void>(`table/${table.id}/${this.sequence}`, obj).pipe(tap(() => this.sequence++));
    }

    public joinTable(tableId: string, playerName: string): Observable<string> {
        const body = {
            name: playerName
        }
        console.log(body);
        return this.httpClient.post<{[id: string]: string}>(`table/${tableId}`, body).pipe(
            map((obj: {[id: string]: string}) => obj.id)
        );
    }

    public leaveTable(tableId: string, playerId: string): Observable<void> {
        return this.httpClient.delete<void>(`table/${tableId}/${playerId}/${this.sequence}`).pipe(tap(() => this.sequence++));
    }
}
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Player } from './player.model';
import { Card } from '../card/card.model';
import { CardService } from '../card/card.service';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit {

  @Input()
  player: Player;

  @Output()
  public cardPlayed: EventEmitter<Card> = new EventEmitter<Card>();
  @Output()
  public sortHand: EventEmitter<void> = new EventEmitter<void>();

  constructor(private cardService: CardService) { }

  ngOnInit(): void {
  }

  public playCard(card: Card): void {
    this.cardPlayed.emit(card);
  }

  public sortCards(): void {
    this.sortHand.emit();
  }
}

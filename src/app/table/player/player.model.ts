import { Card } from '../card/card.model';

export interface Player {
    id: string;
    name: string;
    cards: Card[];
}
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardModule } from '../card/card.module';
import { PlayerComponent } from './player.component';



@NgModule({
  declarations: [PlayerComponent],
  imports: [
    CardModule,
    CommonModule
  ],
  exports: [PlayerComponent]
})
export class PlayerModule { }

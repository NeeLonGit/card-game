import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { TableSectionModule } from './table-section/table-section.module';
import { PlayerModule } from './player/player.module';
import { TableComponent } from './table.component';
import { CardModule } from './card/card.module';

@NgModule({
  declarations: [TableComponent],
  imports: [
    CardModule,
    TableSectionModule,
    PlayerModule,
    CommonModule,
    HttpClientModule
  ],
  exports: [TableComponent],
  providers: []
})
export class TableModule { }

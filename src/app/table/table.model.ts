import { Player } from './player/player.model';
import { TableSection } from './table-section/table-section.model';

export interface Table {
    id: string;
    players: {[playerId: string]: Player };
    mainSection: TableSection;
    sideSection: TableSection;
    playerSections: {[playerId: string]: TableSection };
}
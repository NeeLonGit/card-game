import { ElementFinder } from 'protractor';

export class TablePo {
    constructor(private container: ElementFinder) {}

    public async isDisplayed(): Promise<boolean> {
        return await this.container.isDisplayed();
    }

    public async isNotPresent(): Promise<boolean> {
        return !await this.container.isPresent();
    }
}
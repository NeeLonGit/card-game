import { ElementFinder, browser } from 'protractor';
import { LobbyPo } from './lobby/lobby.po';
import { NameSelectorPo } from './name-selector/name-selector.po';
import { TablePo } from './table/table.po';

export class AppPo {

    private nameSelector: NameSelectorPo;
    private lobby: LobbyPo;
    private table: TablePo;

    constructor(private container: ElementFinder) {
        this.nameSelector = new NameSelectorPo(this.container.$('app-name-selector'));
        this.lobby = new LobbyPo(this.container.$('app-lobby'));
        this.table = new TablePo(this.container.$('app-table'));
    }

    public async happyFlowToNameSelector(): Promise<void> {
        await browser.get('http://localhost:4200/');
    }

    public async happyFlowToLobby(): Promise<void> {
        await browser.get('http://localhost:4200/');
        await this.nameSelector.happyFlow();
    }

    public async happyFlowToNewGame(): Promise<void> {
        await browser.get('http://localhost:4200/');
        await this.nameSelector.happyFlow();
        browser.waitForAngularEnabled(false);
        await this.lobby.happyFlow();
    }

    public getNameSelectorPo(): NameSelectorPo {
        return this.nameSelector;
    }

    public getLobbyPo(): LobbyPo {
        return this.lobby;
    }

    public getTablePo(): TablePo {
        return this.table;
    }
}
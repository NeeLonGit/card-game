import { Component } from '@angular/core';
import { TableResource } from './table/table.resource';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'kaartspel';
  public name: string = '';
  public previousName: string = '';
  public playerId: string = '';
  public tableId: string = '';

  private joinedTables: Map<string, string> = new Map();

  constructor(private tableResource: TableResource) {}

  public clearName(): void {
    this.previousName = this.name;
    this.name = '';
  }

  public leaveTable(): void {
    this.tableResource.leaveTable(this.tableId, this.playerId).subscribe(
      () => {
        this.joinedTables.delete(this.tableId);
        this.playerId = '';
        this.tableId = '';      }
    )
  }

  public joinTable(tableId: string): void {
    this.setTableId(tableId);
    if (this.joinedTables.has(tableId)) {
      this.setPlayerId(this.joinedTables.get(tableId));
      return;
    } else {
      this.tableResource.joinTable(tableId, this.name).subscribe(
        (playerId: string) => {
          this.joinedTables.set(tableId, playerId);
          this.setPlayerId(playerId);
        });
    }
  }

  public hasName(): boolean {
    return !!this.name;
  }

  public setName(name: string): void {
    this.name = name;
  }

  public hasPlayerId(): boolean {
    return !!this.playerId;
  }

  public setPlayerId(playerId: string): void {
    this.playerId = playerId;
  }

  public hasTableId(): boolean {
    return !!this.tableId;
  }

  public setTableId(tableId: string): void {
    this.tableId = tableId;
  }
}
